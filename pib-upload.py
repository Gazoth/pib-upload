import base64
import hashlib
import logging
import os.path
import re
import sys
import time
from datetime import datetime, timezone
from io import BytesIO

import pywikibot
import requests
from bs4 import BeautifulSoup, NavigableString
from PIL import Image
from pywikibot.specialbots import UploadRobot
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

# Constants
urlprefix = 'http://pib.nic.in/newsite/photo.aspx?photoid='
urlprefix_desc = 'http://pib.nic.in/newsite/photoright.aspx?phid='
urlprefix_print = 'http://pib.nic.in/newsite/printphotogallery.aspx?phid='

dup_list = 'logs/duplicates.csv'
failed_list = 'logs/failed.csv'
last_id_file = 'last.txt'
temp_file = 'temp_image'

unknown_date = r'{{other date|?}}'
days = ['Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday', 'Saturday', 'Sunday']
mdy_date = r'(?<=on )(?:January|February|March|April|May|June|July|August|' + \
    r'September|October|November|December) [\d]{1,2}, [\d]{4}'
mdy_date_str = r' on (?:January|February|March|April|May|June|July|August|' + \
    r'September|October|November|December) [\d]{1,2}, [\d]{4}\..*'
ymd_date = r'^[\d]{4}[:-][\d]{1,2}[:-][\d]{1,2}'
null_ymd_date = r'^0000[:-]00[:-]00'
official_title = r'^The (?:(?:Union |Prime )?Minister|President|Chief|Vice' + \
    r'|Governor|Chairman|Founder|CEO|Ambassador|High|Premier).*?\b(?:Dr|Mr' + \
    r'|Miss|Mrs|Ms|Prof|Professor|Shri|Smt)\b\.?\s?'

# Logging configuration
fh = logging.handlers.RotatingFileHandler(filename='logs/debug.log',
                                          maxBytes=5242880,
                                          backupCount=1000)
logging.getLogger().addHandler(fh)

# Retry with exponential backoff
rs = requests.Session()
retries = Retry(total=7,
                backoff_factor=1,
                status_forcelist=[104])
adapter = HTTPAdapter(max_retries=retries)
rs.mount('http://', adapter)
rs.mount('https://', adapter)


def extract_date(string):
    datestr = re.search(mdy_date, string)
    if datestr:
        dt = datetime.strptime(datestr.group(), '%B %d, %Y')
        return dt.strftime('%Y-%m-%d')
    else:
        return unknown_date


def is_date(string):
    if re.search(ymd_date, string) and not re.search(null_ymd_date, string):
        return True
    else:
        return False


def get_max_id():
    r = rs.get(urlprefix)
    soup = BeautifulSoup(r.text, 'html.parser')
    try:
        return int(soup.td['onclick'][:-1].split('(')[1])
    except KeyboardInterrupt:
        raise
    except Exception:
        logging.error('Unable to get the maximum photo ID, using a fallback')
        return 129139


def get_last_id():
    if os.path.isfile(last_id_file):
        try:
            with open(last_id_file, 'r') as f:
                last = int(f.readline())
            return last
        except KeyboardInterrupt:
            raise
        except Exception as e:
            # Unexpected error, log and return fallback
            logging.error(e.args)
    # File does not exist, falling back to first ID
    return 25


def get_institution(dept):
    if 'AYUSH' in dept:
        return '{{Institution:Ministry of AYUSH}}'
    elif dept == 'Cabinet':
        return '{{Institution:Union Cabinet}}'
    elif 'Cabinet Committee on Economic Affairs' in dept:
        return '{{Institution:Cabinet Committee on Economic Affairs}}'
    elif 'Cabinet Committee on Infrastructure' in dept:
        return '{{Institution:Cabinet Committee on Infrastructure}}'
    elif 'Cabinet Committee on Investment' in dept:
        return '{{Institution:Cabinet Committee on Investment}}'
    elif 'Cabinet Committee on Price' in dept:
        return '{{Institution:Cabinet Committee on Price}}'
    elif 'Cabinet Secretariat' in dept:
        return '{{Institution:Cabinet Secretariat (India)}}'
    elif 'Department of Atomic Energy' in dept:
        return '{{Institution:Department of Atomic Energy}}'
    elif 'Department of Ocean Development' in dept:
        return '{{Institution:Department of Ocean Development}}'
    elif 'Department of Space' in dept:
        return '{{Institution:Department of Space (India)}}'
    elif 'Election Commission' in dept:
        return '{{Institution:Election Commission of India}}'
    elif 'Ministry for Development of North-East Region' in dept:
        return '{{Institution:Ministry for Development of North Eastern ' + \
            'Region}}'
    elif 'Ministry of Agriculture & Farmers Welfare' in dept:
        return '{{Institution:Ministry of Agriculture and Farmers\' Welfare}}'
    elif 'Ministry of Agro & Rural Industries' in dept:
        return '{{Institution:Ministry of Agro and Rural Industries}}'
    elif 'Ministry of Chemicals and Fertilizers' in dept:
        return '{{Institution:Ministry of Chemicals and Fertilizers}}'
    elif 'Ministry of Civil Aviation' in dept:
        return '{{Institution:Ministry of Civil Aviation}}'
    elif 'Ministry of Coal' in dept:
        return '{{Institution:Ministry of Coal}}'
    elif 'Ministry of Commerce & Industry' in dept:
        return '{{Institution:Ministry of Commerce and Industry (India)}}'
    elif 'Ministry of Communications' in dept:
        return '{{Institution:Ministry of Communications (India)}}'
    elif 'Ministry of Company Affairs' in dept:
        return '{{Institution:Ministry of Corporate Affairs}}'
    elif 'Ministry of Consumer Affairs, Food & Public Distribution' in dept:
        return '{{Institution:Ministry of Consumer Affairs, Food and ' + \
            'Public Distribution}}'
    elif 'Ministry of Corporate Affairs' in dept:
        return '{{Institution:Ministry of Corporate Affairs}}'
    elif 'Ministry of Culture' in dept:
        return '{{Institution:Ministry of Culture (India)}}'
    elif 'Ministry of Defence' in dept:
        return '{{Institution:Ministry of Defence (India)}}'
    elif 'Ministry of Disinvestment' in dept:
        return '{{Institution:Ministry of Disinvestment}}'
    elif 'Ministry of Drinking Water & Sanitation' in dept:
        return '{{Institution:Ministry of Drinking Water and Sanitation}}'
    elif 'Ministry of Earth Science' in dept:
        return '{{Institution:Ministry of Earth Sciences}}'
    elif 'Ministry of Electronics & IT ' in dept:
        return '{{Institution:Ministry of Electronics and Information ' + \
            'Technology}}'
    elif 'Ministry of Environment, Forest and Climate Change' in dept:
        return '{{Institution:Ministry of Environment, Forest and ' + \
            'Climate Change}}'
    elif 'Ministry of External Affairs' in dept:
        return '{{Institution:Ministry of External Affairs (India)}}'
    elif 'Ministry of Finance' in dept:
        return '{{Institution:Ministry of Finance (India)}}'
    elif 'Ministry of Food Processing Industries' in dept:
        return '{{Institution:Ministry of Food Processing Industries}}'
    elif 'Ministry of Health and Family Welfare' in dept:
        return '{{Institution:Ministry of Health and Family Welfare}}'
    elif 'Ministry of Heavy Industries & Public Enterprises' in dept:
        return '{{Institution:Ministry of Heavy Industries and Public ' + \
            'Enterprises}}'
    elif 'Ministry of Home Affairs' in dept:
        return '{{Institution:Ministry of Home Affairs (India)}}'
    elif 'Ministry of Housing and Urban  Poverty Alleviation' in dept:
        return '{{Institution:Ministry of Housing and Urban Poverty ' + \
            'Alleviation}}'
    elif 'Ministry of Housing & Urban Affairs' in dept:
        return '{{Institution:Ministry of Housing and Urban Affairs}}'
    elif 'Ministry of Human Resource Development' in dept:
        return '{{Institution:Ministry of Human Resource Development}}'
    elif 'Ministry of Information & Broadcasting' in dept:
        return '{{Institution:Ministry of Information and Broadcasting ' + \
            '(India)}}'
    elif 'Ministry of Labour & Employment' in dept:
        return '{{Institution:Ministry of Labour and Employment (India)}}'
    elif 'Ministry of Law & Justice' in dept:
        return '{{Institution:Ministry of Law and Justice (India)}}'
    elif 'Ministry of Micro, Small & Medium Enterprises' in dept:
        return '{{Institution:Ministry of Micro, Small and Medium ' + \
            'Enterprises}}'
    elif 'Ministry of Mines' in dept:
        return '{{Institution:Ministry of Mines (India)}}'
    elif 'Ministry of Minority Affairs' in dept:
        return '{{Institution:Ministry of Minority Affairs}}'
    elif 'Ministry of New and Renewable Energy ' in dept:
        return '{{Institution:Ministry of New and Renewable Energy}}'
    elif 'Ministry of Overseas Indian Affairs' in dept:
        return '{{Institution:Ministry of Overseas Indian Affairs}}'
    elif 'Ministry of Panchayati Raj' in dept:
        return '{{Institution:Ministry of Panchayati Raj}}'
    elif 'Ministry of Parliamentary Affairs' in dept:
        return '{{Institution:Ministry of Parliamentary Affairs (India)}}'
    elif 'Ministry of Personnel, Public Grievances & Pensions' in dept:
        return '{{Institution:Ministry of Personnel, Public Grievances ' + \
            'and Pensions}}'
    elif 'Ministry of Petroleum & Natural Gas' in dept:
        return '{{Institution:Ministry of Petroleum and Natural Gas}}'
    elif 'Ministry of Planning' in dept:
        return '{{Institution:Ministry of Planning}}'
    elif 'Ministry of Power' in dept:
        return '{{Institution:Ministry of Power (India)}}'
    elif 'Ministry of Railways' in dept:
        return '{{Institution:Ministry of Railways (India)}}'
    elif 'Ministry of Road Transport & Highways' in dept:
        return '{{Institution:Ministry of Petroleum and Natural Gas}}'
    elif 'Ministry of Rural Development' in dept:
        return '{{Institution:Ministry of Rural Development (India)}}'
    elif 'Ministry of Science & Technology' in dept:
        return '{{Institution:Ministry of Science and Technology (India)}}'
    elif 'Ministry of Shipping' in dept:
        return '{{Institution:Ministry of Shipping (India)}}'
    elif 'Ministry of Skill Development and Entrepreneurship' in dept:
        return '{{Institution:Ministry of Skill Development and ' + \
            'Entrepreneurship}}'
    elif 'Ministry of Social Justice & Empowerment' in dept:
        return '{{Institution:Ministry of Social Justice and Empowerment}}'
    elif 'Ministry of Statistics & Programme Implementation' in dept:
        return '{{Institution:Ministry of Statistics and Programme ' + \
            'Implementation}}'
    elif 'Ministry of Steel' in dept:
        return '{{Institution:Ministry of Steel}}'
    elif 'Ministry of Surface Transport' in dept:
        return '{{Institution:Ministry of Surface Transport}}'
    elif 'Ministry of Textiles' in dept:
        return '{{Institution:Ministry of Textiles}}'
    elif 'Ministry of Tourism' in dept:
        return '{{Institution:Ministry of Tourism (India)}}'
    elif 'Ministry of Tribal Affairs' in dept:
        return '{{Institution:Ministry of Tribal Affairs}}'
    elif 'Ministry of Water Resources' in dept:
        return '{{Institution:Ministry of Water Resources (India)}}'
    elif 'Ministry of Women and Child Development' in dept:
        return '{{Institution:Ministry of Women and Child Development}}'
    elif 'Ministry of Youth Affairs and Sports' in dept:
        return '{{Institution:Ministry of Youth Affairs and Sports}}'
    elif 'NITI Aayog' in dept:
        return '{{Institution:NITI Aayog}}'
    elif 'Planning Commission' in dept:
        return '{{Institution:Planning Commission (India)}}'
    elif 'PMEAC' in dept:
        return '{{Institution:Economic Advisory Council to the Prime ' + \
            'Minister}}'
    elif 'Vice President\'s Secretariat' in dept:
        return '{{Institution:Vice President\'s Secretariat}}'
    elif 'President\'s Secretariat' in dept:
        return '{{Institution:President\'s Secretariat}}'
    elif 'Prime Minister\'s Office' in dept:
        return '{{Institution:Prime Minister\'s Office (India)}}'
    elif 'UPSC' in dept:
        return '{{Institution:Union Public Service Commission}}'
    elif 'Union Cabinet' in dept:
        return '{{Institution:Union Cabinet}}'
    # No need to create pages for these two
    # elif 'Cabinet Committee Decisions' in dept:
    #    return '{{Institution:Cabinet Committee Decisions}}'
    # elif 'Other Cabinet Committees' in dept:
    #    return '{{Institution:Other Cabinet Committees}}'
    elif dept:
        return dept + ', ' + 'Government of India'
    else:
        return 'Government of India'


def categorise(caption, date):
    cat = ''
    split_by_month = ['Narendra Modi']
    split_by_year = ['Manmohan Singh', 'Pratibha Patil', 'Pranab Mukherjee']
    for name in split_by_month:
        if name in caption:
            if date == unknown_date:
                cat += '[[Category:' + name + ']]\n'
            else:
                parsed_date = datetime.strptime(date[:10], r'%Y-%m-%d')
                cat += '[[Category:' + name + ' in ' + \
                    parsed_date.strftime(r'%B %Y') + ']]\n'
    for name in split_by_year:
        if name in caption:
            if date == unknown_date:
                cat += '[[Category:' + name + ']]\n'
            else:
                parsed_date = datetime.strptime(date[:10], r'%Y-%m-%d')
                cat += '[[Category:' + name + ' in ' + \
                    parsed_date.strftime(r'%Y') + ']]\n'
    return cat


def upload_single(phid):
    # Remove any leftover file
    if os.path.exists(temp_file):
        os.remove(temp_file)

    phid_str = str(phid)
    r = rs.get(urlprefix_desc + phid_str)
    soup = BeautifulSoup(r.text, 'html.parser')
    image_tag = soup.find(id='dimage', href=True)
    if image_tag is None:
        # Some IDs do not return valid pages. Example: 108119
        logging.info('Photo ID %s did not return a valid page' % phid_str)
        return

    image_url = image_tag['href'].strip()
    if "WriteReadData" in image_url:
        # New image, has a new ID too. Example: 128273
        new_id = image_url[image_url.rfind('/')+10:image_url.rfind('.')]
    else:
        new_id = ''

    dept = soup.find(id='ministry').string.strip()
    if dept.endswith('.'):
        dept = dept[:-1].strip()
    caption_id_list = soup.find(id='caption').contents
    file_name = caption_id_list[2].string.strip()

    # If file_name has "National Photo Contest" in name, ignore as likely
    # copyvio. Log in failed list for manual review
    if 'National Photo Contest' in file_name:
        with open(failed_list, 'a') as f:
            f.writelines(urlprefix + phid_str + '\t' +
                         image_url + '\t' + 'NPC' + '\n')
        return

    # Remove characters that are invalid for a MediaWiki title
    file_name = re.sub(r'[#<>\[\]\|\{\}_/:\\]', '', file_name)
    file_name = re.sub(r'[\r\n\t]', ' ', file_name)

    # Trim day of the week from suffix
    if file_name[file_name.rfind('(')+1:file_name.rfind(')')] in days:
        file_name = file_name[0:file_name.rfind('(')].strip()

    # Convert all caps to something more sensible
    if file_name.isupper():
        file_name = file_name.title()

    # Commons has file name length limit of 240, reserving 15 chars for name
    # conflict resolution and file extension. Example with long caption: 18209
    if len(file_name) > 225:
        # Strip the date
        file_name = re.sub(mdy_date_str, '', file_name)

    # If that wasn't enough, strip the title
    if len(file_name) > 225:
        file_name = re.sub(official_title, '', file_name)

    # If that wasn't enough, trim using NL, comma or space delim
    if len(file_name) > 225:
        trimmed_name = file_name[0:225]

        # Using new line as a separator, get the largest possible sentence
        # Example: 101758
        nlidx = trimmed_name.rfind('\n')
        if nlidx != -1:
            trimmed_name = trimmed_name[0:nlidx].strip()
        else:
            # Using comma as a separator, get the largest possible
            # sentence fragment. Example: 11848
            cidx = trimmed_name.rfind(',')
            if cidx > 80:
                # Long enough, is probably good
                trimmed_name = trimmed_name[0:cidx]
            else:
                # No graceful split possible, trying a split with space delim
                sidx = trimmed_name.rfind(' ')
                if sidx != -1:
                    trimmed_name = trimmed_name[0:sidx]
                # If file_name now ends in punctuations, trim it out too
                if trimmed_name[-1] in ['!', '&']:
                    trimmed_name = trimmed_name[0:-1].strip()

        logging.info('File name for photo ID ' + phid_str +
                     ' was trimmed to "' + trimmed_name + '"')
        file_name = trimmed_name

    # Get file extension, remove full stop from file_name if present
    eidx = image_url.rfind('.')
    file_ext = image_url[eidx:]
    if file_name.endswith('.'):
        file_name = file_name[:-1]

    # Caption may span multiple sentences. Example: 18209
    caption = caption_id_list[2].string.strip()
    for i in range(3, len(caption_id_list) - 1):
        if isinstance(caption_id_list[i], NavigableString):
            caption += ' ' + caption_id_list[i].string.strip()

    # Get date from EXIF, failing which try to extract it from caption
    time.sleep(1)
    img = rs.get(image_url)
    if img.status_code == 200:
        with open(temp_file, 'wb') as f:
            f.write(img.content)
    else:
        with open(failed_list, 'a') as f:
            f.writelines(urlprefix + phid_str + '\t' + image_url + '\n')
        return
    try:
        exif_dict = Image.open(BytesIO(img.content))._getexif()
        if not exif_dict:
            date = extract_date(caption)
        elif 0x9003 in exif_dict and is_date(exif_dict[0x9003]):
            date = exif_dict[0x9003]
        elif 0x9004 in exif_dict and is_date(exif_dict[0x9004]):
            date = exif_dict[0x9004]
        elif 0x0132 in exif_dict and is_date(exif_dict[0x0132]):
            date = exif_dict[0x0132]
        else:
            date = extract_date(caption)
    except KeyboardInterrupt:
        raise
    except OSError:
        # Image file is improper, log and exit
        with open(failed_list, 'a') as f:
            f.writelines(urlprefix + phid_str + '\t' + image_url + '\n')
        return
    except Exception:
        date = extract_date(caption)
    # Format date
    date = re.sub(r'([0-9]{4}):([0-9]{2}):([0-9]{2})', r'\1-\2-\3', date)

    # Check for Hindi caption. Example: 110370
    time.sleep(1)
    c = rs.get(urlprefix_print + phid_str)
    cs = BeautifulSoup(c.text, 'html.parser')
    if cs.span and cs.span.string:
        # Add Hindi to caption
        caption += '}}\n{{hi|1=' + cs.span.string.replace("|", "{{!}}")

    # Last element has the IDs
    if not caption_id_list[-1].string:
        # Unexpected format, log as failed
        with open(failed_list, 'a') as f:
            f.writelines(urlprefix + phid_str + '\t' + image_url + '\n')
        return

    ids = caption_id_list[-1].string.split(" ")
    cnr = ids[1][1:]
    if cnr == '0':
        # CNR doesn't exist
        cnr = ''
    photo_id = ids[4][1:]

    summary = 'Uploading Press Information Bureau image with ID ' + phid_str
    dt = datetime.now(timezone.utc)
    desc = ('=={{int:filedesc}}==\n'
            '{{Information\n'
            '|description={{en|1=' + caption + '}}\n'
            '|date=' + date + '\n'
            '|source={{ID-PIB|idv1=' + photo_id + '|idv2=' + new_id +
            '|cnr=' + cnr + '|dept=' + dept + '|url=' + image_url + '}}\n'
            '|author=' + get_institution(dept) + '\n'
            '|permission=\n'
            '|other versions=\n'
            '}}\n\n'
            '=={{int:license-header}}==\n'
            '{{GODL-India' + ('|' + dept if dept else '') + '|url=' +
            urlprefix + phid_str + '|status=confirmed|reviewer=GazothBot|date='
            + dt.strftime("%Y-%m-%d") + '}}\n'
            '\n' + categorise(caption, date) +
            dt.strftime("{{Uncategorized-PIB|year=%Y|month=%B|day=%-d}}\n"))

    site = pywikibot.Site('commons', 'commons')

    sha1 = hashlib.sha1()
    sha1.update(BytesIO(img.content).getvalue())
    duplicate = False
    for page in site.allimages(sha1=base64.b16encode(sha1.digest())):
        duplicate = True
        with open(dup_list, 'a') as f:
            f.writelines(page.title(underscore=True) + '\t' + '{{ID-PIB|idv1='
                         + photo_id + '|idv2=' + new_id + '|cnr=' + cnr +
                         '|dept=' + dept + '|url=' + image_url + '}}\n')

    if duplicate:
        return

    # Find a file name that doesn't clash
    time.sleep(1)
    if pywikibot.Page(site, title='File:' + file_name + file_ext).exists():
        dab = 1
        while pywikibot.Page(site, title='File:' + file_name + ' (' +
                             str(dab) + ')' + file_ext).exists():
            time.sleep(1)
            dab += 1
            if (dab > 99):
                logging.error(
                    'Unable to find a valid file name for ' + phid_str)
                return
        file_name += ' (' + str(dab) + ')'

    bot = UploadRobot([temp_file], urlEncoding=None,
                      description=desc,
                      useFilename=file_name + file_ext,
                      always=True,
                      aborts=True,
                      targetSite=site,
                      summary=summary,
                      chunk_size=1 << 20)
    bot.run()
    os.remove(temp_file)


last_id = get_last_id()
max_id = get_max_id()

try:
    while last_id < max_id:
        logging.info('Initiating upload of image ID ' + str(last_id + 1))
        upload_single(last_id + 1)
        time.sleep(2)
        last_id = last_id + 1

except Exception as e:
    if isinstance(e, KeyboardInterrupt):
        sys.exit()
    logging.error(e.args)

finally:
    with open(last_id_file, 'w') as f:
        f.write(str(last_id))
